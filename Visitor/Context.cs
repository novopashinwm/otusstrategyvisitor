using System;

namespace Visitor
{
    public class Context
    {
        private IExport export;

        public Context(IExport export)
        {
            this.export = export;
        }

        // Можно было сразу принимать в качестве аргумента инстанс базового типа, а уже внутри определять
        // более конкретный его тип.
        public string DoExport(Shape shape)
        {
            if (shape is Point) return export.Export((Point) shape);
            else if (shape is Circle) return export.Export((Circle) shape);
            else if (shape is Square) return export.Export((Square) shape);
            throw new Exception("Не удалось найти нужную фигуру!!!");
        }
    }
}