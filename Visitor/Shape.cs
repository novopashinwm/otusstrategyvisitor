namespace Visitor
{
    public abstract class Shape
    {
        // Поля класса лучше делать публичными и readonly, если не предполагается их
        // дальнейшее изменение
        public readonly int x;
        public readonly int y;

        public Shape(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public abstract string Export(IVisitor visitor);
    }
}