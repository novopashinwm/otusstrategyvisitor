namespace Visitor
{
    public class Square : Shape
    {
        public int side;
        public Square(int x, int y, int side) 
            : base(x, y)
        {
            this.side = side;
        }

        public override string Export(IVisitor visitor)
            => visitor.Export(this);
    }
}