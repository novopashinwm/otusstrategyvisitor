namespace Visitor
{
    public interface IVisitor
    {
        string Export(Point point);
        string Export(Circle circle);
        string Export(Square square);
    }
}