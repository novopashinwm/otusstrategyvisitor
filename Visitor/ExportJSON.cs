namespace Visitor
{
    public class ExportJSON : IExport
    {
        public string Export(Point point)
        {
            return  $@"{{""X"":{point.x}, ""Y"":{point.y}}}";
        }

        public string Export(Circle circle)
        {
            return  $@"{{""X"":{circle.x}, ""Y"":{circle.y}, ""R"":{circle.radius}}}";
        }

        public string Export(Square square)
        {
            return  $@"{{""X"":{square.x}, ""Y"":{square.y}, ""R"":{square.side}}}";
        }
    }
}