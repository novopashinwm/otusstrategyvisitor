namespace Visitor
{
    public class VisitorXML: IVisitor
    {
        public string Export(Shape shape)
        {
            
            // Для названия класса в строке можно использовать nameof()
            return
$@"<{shape.GetType().Name}>
    <X>{shape.x}</X>
    <Y>{shape.y}</Y>
</{shape.GetType().Name}>";        
        }
        
        public string Export(Point point)
        {
            return
$@"<{point.GetType().Name}>
    <X>{point.x}</X>
    <Y>{point.y}</Y>
</{point.GetType().Name}>";        
        }

        public string Export(Circle circle)
        {
            return
$@"<{circle.GetType().Name}>
    <X>{circle.x}</X>
    <Y>{circle.y}</Y>
    <R>{circle.radius}</R>
</{circle.GetType().Name}>"; 
        }

        public string Export(Square square)
        {
            return
$@"<{square.GetType().Name}>
    <X>{square.x}</X>
    <Y>{square.y}</Y>
   <SIDE>{square.side}</SIDE>
</{square.GetType().Name}>"; 
        }
    }
}