﻿using System;
using System.Runtime.InteropServices;

namespace Visitor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Point point = new Point(100, 200);
            Circle circle = new Circle(20,50,10);
            Square square = new Square(10,10, 40);

            Console.WriteLine("Реализация STRATEGY");
            
            Context context = new Context(new ExportXML());
            Console.WriteLine(context.DoExport(point));
            Console.WriteLine(context.DoExport(circle));
            Console.WriteLine(context.DoExport(square));

            context = new Context(new ExportJSON());
            Console.WriteLine(context.DoExport(point));
            Console.WriteLine(context.DoExport(circle));
            Console.WriteLine(context.DoExport(square));
            Console.WriteLine();
            
            Console.WriteLine("РЕАЛИЗАЦИЯ VISITOR");
            IVisitor visXML = new VisitorXML();
            IVisitor visJSON = new VisitorJSON();

            Console.WriteLine(circle.Export(visXML));
            Console.WriteLine(point.Export(visXML));
            Console.WriteLine(square.Export(visXML));
            
            Console.WriteLine(circle.Export(visJSON));
            Console.WriteLine(point.Export(visJSON));
            Console.WriteLine(square.Export(visJSON));
            
            Console.ReadKey();
        }
    }
}