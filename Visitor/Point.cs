namespace Visitor
{
    public class Point : Shape
    {
        public Point(int x, int y) : base(x, y)
        {
        }

        public override string Export(IVisitor visitor)
            => visitor.Export(this);
    }
}